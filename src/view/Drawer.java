package view;

import controller.CharacterController;
import controller.ObjectController;
import controller.PiecesController;
import utils.Constants;
import utils.ResourcesConstants;
import utils.Utils;

import java.awt.*;

/**
 * Created by ste on 18/03/17.
 */
public class Drawer {
    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;
    private Image imgFlag;
    private Image imgCastle;
    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;



    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public int getBackground1PosX () { return background1PosX;}

    public int getBackground2PosX () {return  background2PosX;}


    public void drawScore(Graphics g2, int score) {
        g2.drawString(score + " Points", Constants.OFFSET_SCORE_X, Constants.OFFSET_SCORE_Y);
    }

    private void variablesInitialization() {
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = Constants.INITIAL_POSITION;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
    }

    public void initialization() {
        variablesInitialization();
        backgroundInitialization();
    }


    private void drawTurtle(Graphics g2, CharacterController characterController) {
        if (characterController.turtle.isAlive())
            g2.drawImage(characterController.turtle.walk(ResourcesConstants.IMGP_CHARACTER_TURTLE, Constants.TURTLE_FREQUENCY), characterController.turtle.getX(), characterController.turtle.getY(), null);
        else
            g2.drawImage(characterController.turtle.deadImage(), characterController.turtle.getX(), characterController.turtle.getY() + Constants.TURTLE_DEAD_OFFSET_Y, null);
    }

    private void drawMushroom(Graphics g2, CharacterController characterController) {
        if (characterController.mushroom.isAlive())
            g2.drawImage(characterController.mushroom.walk(ResourcesConstants.IMGP_CHARACTER_MUSHROOM, Constants.MUSHROOM_FREQUENCY), characterController.mushroom.getX(), characterController.mushroom.getY(), null);
        else
            g2.drawImage(characterController.mushroom.deadImage(), characterController.mushroom.getX(), characterController.mushroom.getY() + Constants.MUSHROOM_DEAD_OFFSET_Y, null);
    }


    private void drawBackground(Graphics g2) {
        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);
    }

    private void drawObjects(Graphics g2, ObjectController objectController) {
        for (int i = 0; i < objectController.getElements().size(); i++) {
            g2.drawImage(objectController.getElements().get(i).getImgObj(), objectController.getElements().get(i).getX(),
                    objectController.getElements().get(i).getY(), null);
        }
    }

    private void drawPieces(Graphics g2, PiecesController piecesController) {
        for (int i = 0; i < piecesController.getElements().size(); i++) {
            g2.drawImage(piecesController.getElements().get(i).imageOnMovement(), piecesController.getElements().get(i).getX(),
                    piecesController.getElements().get(i).getY(), null);
        }
    }

    private void drawCastleAndFlag(Graphics g2) {
        g2.drawImage(this.imgFlag, Constants.FLAG_X_POS - this.xPos, Constants.FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, Constants.CASTLE_X_POS - this.xPos, Constants.CASTLE_Y_POS, null);
    }

    private void drawMarioMoving(Graphics g2, CharacterController characterController) {
        if (characterController.mario.isJumping())
            g2.drawImage(characterController.mario.doJump(), characterController.mario.getX(), characterController.mario.getY(), null);
        else
            g2.drawImage(characterController.mario.walk(ResourcesConstants.IMGP_CHARACTER_MARIO, Constants.MARIO_FREQUENCY), characterController.mario.getX(), characterController.mario.getY(), null);
    }

    public void backgroundInitialization() {
        this.imgBackground1 = Utils.getImage(ResourcesConstants.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(ResourcesConstants.IMG_BACKGROUND);
        this.castle = Utils.getImage(ResourcesConstants.IMG_CASTLE);
        this.start = Utils.getImage(ResourcesConstants.START_ICON);
    }

    public void setCastleAndFlag() {
        this.imgCastle = Utils.getImage(ResourcesConstants.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(ResourcesConstants.IMG_FLAG);
    }

    public void drawImages(Graphics g2, CharacterController characterController, ObjectController objectController, PiecesController piecesController) {
        drawBackground(g2);
        drawObjects(g2, objectController);
        drawPieces(g2, piecesController);
        drawCastleAndFlag(g2);
        drawMarioMoving(g2, characterController);
        drawMushroom(g2, characterController);
        drawTurtle(g2, characterController);
    }

}
