package controller;
import java.util.ArrayList;

import models.objects.Block;
import models.objects.GameObject;
import models.objects.Tunnel;

/**
 * Created by ste on 18/03/17.
 */
public class ObjectController {
    public static final int TUNNEL_POSITION_Y = 230;

    public ArrayList<GameObject> elements;

    public void create() {
        elements = new ArrayList<GameObject>();
        createBlocks();
        createTunnels();
    }

    private void createTunnels() {
        this.elements.add(new Tunnel(600, TUNNEL_POSITION_Y));
        this.elements.add(new Tunnel(1000, TUNNEL_POSITION_Y));
        this.elements.add(new Tunnel(1600, TUNNEL_POSITION_Y));
        this.elements.add(new Tunnel(1900, TUNNEL_POSITION_Y));
        this.elements.add(new Tunnel(2500, TUNNEL_POSITION_Y));
        this.elements.add(new Tunnel(3000, TUNNEL_POSITION_Y));
        this.elements.add(new Tunnel(3800, TUNNEL_POSITION_Y));
        this.elements.add(new Tunnel(4500, TUNNEL_POSITION_Y));
    }

    private void createBlocks() {
        this.elements.add(new Block(400, 180));
        this.elements.add(new Block(1200, 180));
        this.elements.add(new Block(1270, 170));
        this.elements.add(new Block(1340, 160));
        this.elements.add(new Block(2000, 180));
        this.elements.add(new Block(2600, 160));
        this.elements.add(new Block(2650, 180));
        this.elements.add(new Block(3500, 160));
        this.elements.add(new Block(3550, 140));
        this.elements.add(new Block(4000, 170));
        this.elements.add(new Block(4200, 200));
        this.elements.add(new Block(4300, 210));
    }

    public ArrayList<GameObject> getElements() {
        return elements;
    }

    public void reset() {
        this.elements.clear();
        this.elements = null;
    }

    public void move() {
        for (int i = 0; i < elements.size(); i++) {
            elements.get(i).move();
        }
    }
}
