package controller;

import models.objects.Piece;

import java.util.ArrayList;

/**
 * Created by ste on 18/03/17.
 */
public class PiecesController {

    public ArrayList<Piece> elements;

    public void createPieces() {
        elements = new ArrayList<Piece>();
        this.elements.add(new Piece(402, 145));
        this.elements.add(new Piece(1202, 140));
        this.elements.add(new Piece(1272, 95));
        this.elements.add(new Piece(1342, 40));
        this.elements.add(new Piece(1650, 145));
        this.elements.add(new Piece(2650, 145));
        this.elements.add(new Piece(3000, 135));
        this.elements.add(new Piece(3400, 125));
        this.elements.add(new Piece(4200, 145));
        this.elements.add(new Piece(4600, 40));
    }

    public void reset() {
        this.elements.clear();
        this.elements = null;
    }

    public ArrayList<Piece> getElements() {
        return elements;
    }

    public void move() {
        for (int i = 0; i < elements.size(); i++) {
            elements.get(i).move();
        }
    }

}
