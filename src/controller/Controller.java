package controller;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.*;

import controller.eventManagerOperatingSystem.Audio;
import controller.eventManagerOperatingSystem.Keyboard;
import utils.Constants;
import utils.ResourcesConstants;
import view.Drawer;


@SuppressWarnings("serial")
public class Controller extends JPanel {



    public ObjectController objectController;
    public PiecesController piecesController;
    public CharacterController characterController;
    public Drawer drawer;
    public MoveController moveController;

    public Controller() {
        super();
        piecesController = new PiecesController();
        objectController = new ObjectController();
        characterController = new CharacterController();
        drawer = new Drawer();
        moveController = new MoveController();
        drawer.initialization();
        creation();
        drawer.setCastleAndFlag();
        setFocusAndKeyboard();
    }

    private void creation() {
        objectController.create();
        piecesController.createPieces();
        characterController.createCharacters();
    }

    private void setFocusAndKeyboard() {
        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addKeyListener(new Keyboard());
    }

    private void restart() {
        objectController.reset();
        piecesController.reset();
        characterController.deleteAll();
        creation();
        drawer.initialization();
    }

    private void checkMario() {
        if(!characterController.checkMario()) {
            restart();
        }
    }



    private boolean checkEnd() {
        if (characterController.mario.getX() > Constants.FLAG_X_POS) {
            return true;
        }
        return false;
    }

    public void paintComponent(Graphics g) {
        if(!checkEnd()) {
            checkMario();
            super.paintComponent(g);
            Graphics g2 = (Graphics2D) g;
            characterController.contactsDetection(objectController.getElements(), piecesController.getElements());
            moveController.moveFixedObjects(characterController, objectController, piecesController);
            drawer.drawImages(g2, characterController, objectController, piecesController);
            drawer.drawScore(g2, characterController.getScore());
        } else {
            Audio.playSound(ResourcesConstants.AUDIO_END);
        }
    }
}
