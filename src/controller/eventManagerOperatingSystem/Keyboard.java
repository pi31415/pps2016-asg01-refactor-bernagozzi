package controller.eventManagerOperatingSystem;

import controller.Main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    private final int background1PosX = -50;
    private final int background2PosX = 750;

    private void setBackgroundPosition() {
        Main.scene.drawer.setBackground1PosX(this.background1PosX);
        Main.scene.drawer.setBackground2PosX(this.background2PosX);
    }

    private void blockCastleAndStart(boolean toRight) {
        if (toRight) {
            if (Main.scene.drawer.getxPos() == -1) {
                Main.scene.drawer.setxPos(0);
            }
        }
        else {
            if (Main.scene.drawer.getxPos() == 4601) {
                Main.scene.drawer.setxPos(4600);
            }
        }
        setBackgroundPosition();
    }

    private void setMarioMoving(boolean toRight) {
        int direction = -1;
        if (toRight) {
            direction = 1;
        }
        Main.scene.characterController.mario.setMoving(true);
        Main.scene.characterController.mario.setToRight(toRight);
        Main.scene.drawer.setMov(direction);
    }

    private void marioJump() {
        Main.scene.characterController.mario.setJumping(true);
        Audio.playSound("/resources/audio/jump.wav");
    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.scene.characterController.mario.isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                blockCastleAndStart(true);
                setMarioMoving(true);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                blockCastleAndStart(false);
                setMarioMoving(false);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                marioJump();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.scene.characterController.mario.setMoving(false);
        Main.scene.drawer.setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
