package controller;

import controller.eventManagerOperatingSystem.Audio;
import models.characters.Mario;
import models.characters.Mushroom;
import models.characters.Turtle;
import models.objects.GameObject;
import models.objects.Piece;
import models.objects.Score;
import utils.ResourcesConstants;

import java.util.ArrayList;

/**
 * Created by ste on 18/03/17.
 */
public class CharacterController {


    public Mario mario;
    public Mushroom mushroom;
    public Turtle turtle;
    public Score score;

    public void createCharacters () {
        this.mario = new Mario(300, 245);
        this.mushroom = new Mushroom(800, 263);
        this.turtle = new Turtle(950, 243);
        this.score = new Score();
    }

    private void detectContacCharacterObject(ArrayList<GameObject> objects) {
        for (int i = 0; i < objects.size(); i++) {
            if (this.mario.isNearby(objects.get(i)))
                this.mario.contact(objects.get(i));

            if (this.mushroom.isNearby(objects.get(i)))
                this.mushroom.contact(objects.get(i));

            if (this.turtle.isNearby(objects.get(i)))
                this.turtle.contact(objects.get(i));
        }
    }

    public boolean checkMario() {
        return this.mario.isAlive();
    }

    private void detectContactsBetweenCharcters() {
        if (this.mushroom.isNearby(turtle)) {
            this.mushroom.contact(turtle);
        }
        if (this.turtle.isNearby(mushroom)) {
            this.turtle.contact(mushroom);
        }
        if (this.mario.isNearby(mushroom)) {
            this.mario.contact(mushroom);
        }
        if (this.mario.isNearby(turtle)) {
            this.mario.contact(turtle);
        }
    }

    private void detectContactMarioPieces(ArrayList<Piece> pieces) {
        for (int i = 0; i < pieces.size(); i++) {
            if (this.mario.contactPiece(pieces.get(i))) {
                Audio.playSound(ResourcesConstants.AUDIO_MONEY);
                pieces.remove(i);
                this.score.increase();
            }
        }
    }

    public void contactsDetection(ArrayList<GameObject> objects, ArrayList<Piece> pieces) {
        detectContacCharacterObject(objects);
        detectContactMarioPieces(pieces);
        detectContactsBetweenCharcters();
    }

    public void deleteAll() {
        this.mario = null;
        this.mushroom = null;
        this.turtle = null;
        this.score.reset();
    }

    public int getScore() {
        return this.score.getScore();
    }

    public void move() {
        this.mushroom.move();
        this.turtle.move();
    }
}
