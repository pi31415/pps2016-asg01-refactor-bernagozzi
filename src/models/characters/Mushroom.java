package models.characters;

import java.awt.Image;

import utils.ResourcesConstants;
import utils.Utils;

public class Mushroom extends BasicEnemy implements Runnable {

    public static final int WIDTH = 27;
    public static final int HEIGHT = 30;



    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.img = Utils.getImage(ResourcesConstants.IMG_MUSHROOM_DEFAULT);
        Thread chronoMushroom = new Thread(this);
        chronoMushroom.start();
    }

    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? ResourcesConstants.IMG_MUSHROOM_DEAD_DX : ResourcesConstants.IMG_MUSHROOM_DEAD_SX);
    }
}
