package models.characters;

import java.awt.Image;

import controller.Main;
import models.objects.GameObject;
import models.objects.Piece;
import utils.ResourcesConstants;
import utils.Utils;

public class Mario extends BasicCharacter {

    public static final int MARIO_OFFSET_Y_INITIAL = 243;
    public static final int FLOOR_OFFSET_Y_INITIAL = 293;
    public static final int WIDTH = 28;
    public static final int HEIGHT = 50;
    public static final int JUMPING_LIMIT = 42;

    private Image imgMario;
    private boolean jumping;
    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.imgMario = Utils.getImage(ResourcesConstants.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public boolean isAlive() {
        return this.alive;
    }

    public Image doJump() {
        String str;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > Main.scene.drawer.getHeightLimit())
                this.setY(this.getY() - 4);
            else this.jumpingExtent = JUMPING_LIMIT;
        }
        else if (this.getY() + this.getHeight() < Main.scene.drawer.getFloorOffsetY()) {
            this.setY(this.getY() + 1);
        }
        else {
            this.jumping = false;
            this.jumpingExtent = 0;
        }
        str = this.isToRight() ? ResourcesConstants.IMG_MARIO_SUPER_DX : ResourcesConstants.IMG_MARIO_SUPER_SX;
        return Utils.getImage(str);
    }

    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.isToRight() || this.hitBack(obj) && !this.isToRight()) {
            Main.scene.drawer.setMov(0);
            this.setMoving(false);
        }

        if (this.hitBelow(obj) && this.jumping) {
            Main.scene.drawer.setFloorOffsetY(obj.getY());
        } else if (!this.hitBelow(obj)) {
            Main.scene.drawer.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(obj)) {
                Main.scene.drawer.setHeightLimit(obj.getY() + obj.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(obj) && !this.jumping) {
                Main.scene.drawer.setHeightLimit(0); // initial sky
            }
        }
    }

    public boolean contactPiece(Piece piece) {
        if (this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece) || this.hitBelow(piece)) {
            return true;
        }
        return false;
    }

    private void dead(BasicCharacter pers) {
        pers.setMoving(false);
        pers.setAlive(false);
    }

    private void marioDead() {
        dead(this);
    }

    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) || this.hitBack(pers)) {
            if (pers.alive) {
                marioDead();
            } else this.alive = true;
        } else if (this.hitBelow(pers)) {
            dead(pers);
        }
    }
}

