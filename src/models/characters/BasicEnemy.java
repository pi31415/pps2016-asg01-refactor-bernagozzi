package models.characters;

import models.objects.GameObject;

import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

/**
 * Created by ste on 18/03/2017.
 */
public abstract class BasicEnemy extends BasicCharacter implements Runnable{

    
    
    protected Image img;
    protected final int PAUSE = 15;
    protected int direction;

    private static final Logger LOGGER = Logger.getLogger(BasicEnemy.class.getName());

    public BasicEnemy(int x, int y, int width, int height) {
        super(x,y,width,height);
        this.setToRight(true);
        this.setMoving(true);
        this.direction = 1;
    }


    protected void setDirection(boolean toRight) {
        this.direction = -1;
        if(toRight) {
            this.direction = 1;
        }
        this.setToRight(toRight);
    }

    //getters
    public Image getImg() {
        return img;
    }

    public void move() {
        this.direction = isToRight() ? 1 : -1;
        this.setX(this.getX() + this.direction);
    }

    @Override
    public void run() {
        while (true) {
            if (this.alive) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                    LOGGER.log(Level.WARNING, "exception in sleeping basic enemy", e);
                }
            }
        }
    }

    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            setDirection(false);
        } else if (this.hitBack(obj) && !this.isToRight()) {
            setDirection(true);
        }
    }

    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) && this.isToRight()) {
            setDirection(false);
        } else if (this.hitBack(pers) && !this.isToRight()) {
            setDirection(true);
        }
    }

}
