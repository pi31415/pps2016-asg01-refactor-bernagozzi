package models.characters;

import java.awt.Image;

import utils.ResourcesConstants;
import utils.Utils;

public class Turtle extends BasicEnemy implements Runnable {

    public static final int WIDTH = 43;
    public static final int HEIGHT = 50;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT);
        this.img = Utils.getImage(ResourcesConstants.IMG_TURTLE_IDLE);
        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
    }

    public Image deadImage() {
        return Utils.getImage(ResourcesConstants.IMG_TURTLE_DEAD);
    }
}
