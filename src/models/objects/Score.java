package models.objects;

/**
 * Created by ste on 18/03/2017.
 */
public class Score {
    private int score;

    public Score() {
        this.reset();
    }

    public void increase() {
        this.score++;
    }

    public void reset() {
        this.score = 0;
    }

    public int getScore() {
        return this.score;
    }
}
